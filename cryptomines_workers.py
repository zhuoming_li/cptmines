from urllib.parse import urlencode
import requests
import debug_util
import heapq
import email_util
import json

def sendReq(gop, url, data, headers):
    furl = url
    print(furl)
    if gop == 'POST':
        response = requests.post(url, data=data, headers=headers)
    elif gop == 'GET':
        response = requests.get(furl, data={}, headers=headers)
    else:
        response = requests.delete(furl, data={}, headers=headers)
    debug_util.debugPrint(response.status_code)
    debug_util.debugPrint(response.json())
    return response


url = 'https://api.cryptomines.app/api/workers'

headers = {
    'Content-type': 'application/json',
}
data = {}

response = sendReq('GET', url, data, headers)

res = response.json()
n = 1
h = []
book = {}
for a in res:
	if a['isSold'] == False:
	    p = int(a['price'])/1000000000000000000
	    mp = int(a['nftData']['minePower'])
	    id = a['marketId']
	    book[id] = {'mp': mp, 'price' : p}
	    ratio = mp / p
	    debug_util.debugPrint(p)
	    debug_util.debugPrint(mp)
	    debug_util.debugPrint(n)
	    debug_util.debugPrint(mp / p)
	    heapq.heappush(h, (ratio, id))
	    n += 1

top3 = heapq.nlargest(3,h)

str = 'Top 3 workers with the best MP / Price ratio: \n'
debug_util.debugPrint(str)
for t in top3:
	content = book[t[1]]
	debug_util.debugPrint(content)
	str += json.dumps(content) + '\n'

email_util.sendMyself("Worker notif", str)
    # for n in a['networkList']:
    #     res_dict[a['coin']].append((n['name'], float(n['withdrawFee'])))